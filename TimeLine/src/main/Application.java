package main;

import java.io.IOException;

import controleur.ControleurFenetreGestion;
import modele.Plateau;
import vue.FenetreGestion;

public class Application {

	public static void main(String[] args) throws IOException {
		new ControleurFenetreGestion(new Plateau(), new FenetreGestion());
	}
}
