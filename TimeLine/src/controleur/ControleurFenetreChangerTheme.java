package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import modele.Plateau;
import modele.enumJeu.ThemeCarte;
import vue.FenetreChangerTheme;

public class ControleurFenetreChangerTheme {

	private Plateau plateau;
	private FenetreChangerTheme fenetreChangerTheme;
	private ControleurPanelJeu controleurPanelJeu;

	public ControleurFenetreChangerTheme(Plateau plateau, FenetreChangerTheme fenetreChangerTheme,
			ControleurPanelJeu controleurPanelJeu) {
		this.plateau = plateau;
		this.fenetreChangerTheme = fenetreChangerTheme;
		this.controleurPanelJeu = controleurPanelJeu;
		this.fenetreChangerTheme.getOk().addActionListener(new OkAction());
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public FenetreChangerTheme getFenetreChangerTheme() {
		return fenetreChangerTheme;
	}

	public void setFenetreChangerTheme(FenetreChangerTheme fenetreChangerTheme) {
		this.fenetreChangerTheme = fenetreChangerTheme;
	}

	public ControleurPanelJeu getControleurPanelJeu() {
		return controleurPanelJeu;
	}

	public void setControleurPanelJeu(ControleurPanelJeu controleurPanelJeu) {
		this.controleurPanelJeu = controleurPanelJeu;
	}

	private class OkAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			plateau.changerTheme((ThemeCarte) fenetreChangerTheme.getComboTheme().getSelectedItem());
			plateau.triTheme();
			controleurPanelJeu.reorganisationTapis();
			fenetreChangerTheme.dispatchEvent(new WindowEvent(fenetreChangerTheme, WindowEvent.WINDOW_CLOSING));
		}
	}
}
