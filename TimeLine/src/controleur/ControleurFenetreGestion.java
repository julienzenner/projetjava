package controleur;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import modele.Joueur;
import modele.Plateau;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;
import vue.FenetreAjoutCarte;
import vue.FenetreGestion;
import vue.FenetrePlateau;

public class ControleurFenetreGestion {

	private Plateau plateau;
	private FenetreGestion fenetre;

	public ControleurFenetreGestion(Plateau plateau, FenetreGestion fenetre) {
		this.plateau = plateau;
		this.fenetre = fenetre;
		this.fenetre.getChoixJeu().addActionListener(new ChoixJeuActionListener());
		this.fenetre.getAjouter().addActionListener(new AjouterActionListener());
		this.fenetre.getModifier().addActionListener(new ModifierActionListener());
		this.fenetre.getSupprimer().addActionListener(new SupprimerActionListener());
		this.fenetre.getAnnuler().addActionListener(new AnnulerActionListener());
		this.fenetre.getEnsembleJoueur().addListSelectionListener(new ListEnsembleJouerActionListener());
		this.fenetre.getLancerPartie().addActionListener(new LancerPartieActionListener());
		this.fenetre.getMenuOuvrir().getMenuItemOuvrir().addActionListener(new MenuItemOuvrirAction());
		this.fenetre.getMenuOuvrir().getMenuAide().addMenuListener(new MenuAideAction());
		this.fenetre.getMenuOuvrir().getMenuItemAjoutCarte().addActionListener(new MenuAjoutAction());
		this.fenetre.addWindowListener(new WindowCloseAction());
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public FenetreGestion getFenetre() {
		return fenetre;
	}

	public void setFenetre(FenetreGestion fenetre) {
		this.fenetre = fenetre;
	}

	private void clearTextField() {
		this.fenetre.getTextDateNaissance().getJFormattedTextField().setText("");
		this.fenetre.getTextPseudo().setText("");
	}

	private class ChoixJeuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (fenetre.getChoixJeu().getSelectedItem().equals(Jeu.timeline)) {
				fenetre.getChoixTheme().setEnabled(false);
			} else {
				fenetre.getChoixTheme().setEnabled(true);
			}
		}
	}

	private class AjouterActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!fenetre.getTextPseudo().getText().isEmpty()) {
				if (!fenetre.getTextDateNaissance().getJFormattedTextField().getText().isEmpty()) {
					try {
						Joueur joueur = new Joueur(fenetre.getTextPseudo().getText(), new SimpleDateFormat("dd/MM/yyyy")
								.parse(fenetre.getTextDateNaissance().getJFormattedTextField().getText()));
						if (!fenetre.getModelListJoueur().contains(joueur)) {
							fenetre.getModelListJoueur().addElement(joueur);
							clearTextField();
						}
					} catch (ParseException e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
	}

	private class ModifierActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!fenetre.getEnsembleJoueur().isSelectionEmpty()) {
				if (!fenetre.getTextPseudo().getText().isEmpty()) {
					if (!fenetre.getTextDateNaissance().getJFormattedTextField().getText().isEmpty()) {
						try {
							Joueur joueur = fenetre.getEnsembleJoueur().getSelectedValue();
							fenetre.getModelListJoueur().removeElement(joueur);
							joueur.setPseudo(fenetre.getTextPseudo().getText());
							joueur.setDateNaissance(new SimpleDateFormat("dd/MM/yyyy")
									.parse(fenetre.getTextDateNaissance().getJFormattedTextField().getText()));
							fenetre.getModelListJoueur().addElement(joueur);
							clearTextField();
						} catch (ParseException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				fenetre.getModifier().setVisible(false);
				fenetre.getSupprimer().setVisible(false);
				fenetre.getAnnuler().setVisible(false);
				fenetre.getAjouter().setVisible(true);
			}
		}
	}

	private class SupprimerActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!fenetre.getEnsembleJoueur().isSelectionEmpty()) {
				int option = JOptionPane.showConfirmDialog(null,
						"Êtes-vous sûre de vouloir supprimer le joueur "
								+ fenetre.getEnsembleJoueur().getSelectedValue().toString() + " ?",
						"Suppression", JOptionPane.YES_NO_OPTION);
				if (option == JOptionPane.YES_OPTION) {
					fenetre.getModelListJoueur().removeElement(fenetre.getEnsembleJoueur().getSelectedValue());
					clearTextField();
					fenetre.getSupprimer().setVisible(false);
					fenetre.getAnnuler().setVisible(false);
					fenetre.getModifier().setVisible(false);
					fenetre.getAjouter().setVisible(true);
				}
			}
		}
	}

	private class AnnulerActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			fenetre.getEnsembleJoueur().clearSelection();
			fenetre.getAnnuler().setVisible(false);
			fenetre.getModifier().setVisible(false);
			fenetre.getSupprimer().setVisible(false);
			fenetre.getAjouter().setVisible(true);
			fenetre.getTextDateNaissance().getJFormattedTextField().setText("");
			fenetre.getTextPseudo().setText("");
		}
	}

	private class ListEnsembleJouerActionListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!fenetre.getEnsembleJoueur().isSelectionEmpty()) {
				fenetre.getTextPseudo().setText(fenetre.getEnsembleJoueur().getSelectedValue().getPseudo());
				fenetre.getTextDateNaissance().getJFormattedTextField().setText(new SimpleDateFormat("dd/MM/yyyy")
						.format(fenetre.getEnsembleJoueur().getSelectedValue().getDateNaissance()));
				fenetre.getAjouter().setVisible(false);
				fenetre.getAnnuler().setVisible(true);
				fenetre.getModifier().setVisible(true);
				fenetre.getSupprimer().setVisible(true);
			}
		}
	}

	private class LancerPartieActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (!fenetre.getModelListJoueur().isEmpty()) {
				if (fenetre.getModelListJoueur().size() > 1 && fenetre.getModelListJoueur().size() < 9) {
					int option = JOptionPane.showConfirmDialog(null, "Êtes-vous sûre de vouloir lancer le jeu ? ",
							"Lancer de partie", JOptionPane.YES_NO_OPTION);
					if (option == JOptionPane.YES_OPTION) {
						try {
							if (plateau.getJoueurs().size() == 0) {
								for (int i = 0; i < fenetre.getModelListJoueur().getSize(); i++) {
									plateau.addJoueurs(fenetre.getModelListJoueur().getElementAt(i));
								}
							} else {
								if (plateau.getJoueurs().size() < fenetre.getModelListJoueur().size()) {
									for (int i = plateau.getJoueurs().size(); i < fenetre.getModelListJoueur()
											.getSize(); i++) {
										plateau.addJoueurs(fenetre.getModelListJoueur().getElementAt(i));
									}
								}
								ArrayList<Joueur> joueurs = new ArrayList<Joueur>(plateau.getJoueurs());
								plateau = new Plateau();
								plateau.setJoueurs(joueurs);
							}
							plateau.triJoueurs();
							plateau.setTypeJeu((Jeu) fenetre.getChoixJeu().getSelectedItem());
							plateau.chargerCarte((ThemeCarte) fenetre.getChoixTheme().getSelectedItem());
							plateau.distribuerCarte();
							plateau.debutPartie();
							new ControleurFenetrePlateau(plateau, new FenetrePlateau(), fenetre);
							fenetre.setVisible(false);
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
						}
					}
				} else {
					JOptionPane.showMessageDialog(null, "Nombre de joueurs inadéquat", "Erreur",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private class MenuItemOuvrirAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			int result = fileChooser.showOpenDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION) {
				try {
					FileInputStream f = new FileInputStream(fileChooser.getSelectedFile());
					ObjectInputStream s = new ObjectInputStream(f);
					plateau = new Plateau((Plateau) s.readObject());
					f.close();
					s.close();
					new ControleurFenetrePlateau(plateau, new FenetrePlateau(), fenetre);
					fenetre.dispatchEvent(new WindowEvent(fenetre, WindowEvent.WINDOW_CLOSING));
				} catch (Exception exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private class MenuAideAction implements MenuListener {

		@Override
		public void menuSelected(MenuEvent e) {
			try {
				Desktop.getDesktop().open(new File("data/regle.pdf"));
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}

		@Override
		public void menuDeselected(MenuEvent e) {
		}

		@Override
		public void menuCanceled(MenuEvent e) {
		}
	}

	private class WindowCloseAction implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
		}

		@Override
		public void windowClosing(WindowEvent e) {
			if (JOptionPane.showConfirmDialog(fenetre, "Êtes-vous sûre de vouloir quitter cette application ?",
					"Quitter l'application ?", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				try {
					if (plateau.getJoueurs().size() > 0) {
						plateau.sauvegarderScores();
					}
					System.exit(0);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}
	}

	private class MenuAjoutAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new ControleurFenetreAjoutCarte(new FenetreAjoutCarte());
		}
	}
}