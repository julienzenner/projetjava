package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import modele.enumJeu.Jeu;
import vue.FenetreAjoutCarte;

public class ControleurFenetreAjoutCarte {

	private FenetreAjoutCarte fenetre;

	public ControleurFenetreAjoutCarte(FenetreAjoutCarte fenetre) {
		this.fenetre = fenetre;
		this.fenetre.getChoixJeu().addActionListener(new ChoixJeuActionListener());
		this.fenetre.getAjouter().addActionListener(new AjouterActionListener());
		this.fenetre.getImageCache().addActionListener(new ButtonImageCacheeAction());
		this.fenetre.getImageVisible().addActionListener(new ButtonImageVisibleAction());
	}

	private class ChoixJeuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (fenetre.getChoixJeu().getSelectedItem() == Jeu.cardline) {
				fenetre.getInvention().setVisible(false);
				fenetre.getTextInvention().setVisible(false);
				fenetre.getDateInvention().setVisible(false);
				fenetre.getTextDateInvention().setVisible(false);
				fenetre.getPays().setVisible(true);
				fenetre.getTextPays().setVisible(true);
				fenetre.getPIB().setVisible(true);
				fenetre.getTextPIB().setVisible(true);
				fenetre.getPollution().setVisible(true);
				fenetre.getTextPollution().setVisible(true);
				fenetre.getSuperficie().setVisible(true);
				fenetre.getTextSuperficie().setVisible(true);
				fenetre.getPopulation().setVisible(true);
				fenetre.getTextPopulation().setVisible(true);
			} else {
				fenetre.getInvention().setVisible(true);
				fenetre.getTextInvention().setVisible(true);
				fenetre.getDateInvention().setVisible(true);
				fenetre.getTextDateInvention().setVisible(true);
				fenetre.getPays().setVisible(false);
				fenetre.getTextPays().setVisible(false);
				fenetre.getPIB().setVisible(false);
				fenetre.getTextPIB().setVisible(false);
				fenetre.getPollution().setVisible(false);
				fenetre.getTextPollution().setVisible(false);
				fenetre.getSuperficie().setVisible(false);
				fenetre.getTextSuperficie().setVisible(false);
				fenetre.getPopulation().setVisible(false);
				fenetre.getTextPopulation().setVisible(false);
			}
		}
	}

	private class ButtonImageCacheeAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			int result = fileChooser.showOpenDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION && fenetre.getNomImage().getText().isEmpty() == false) {
				if (fenetre.getChoixJeu().getSelectedItem() == Jeu.timeline) {
					fileChooser.getSelectedFile()
							.renameTo(new File("data/timeline/cards/" + fenetre.getTextNomImage().getText() + ".jpeg"));
				} else {
					fileChooser.getSelectedFile()
							.renameTo(new File("data/cardline/cards/" + fenetre.getTextNomImage().getText() + ".jpeg"));
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!", "Informations",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	private class ButtonImageVisibleAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			int result = fileChooser.showOpenDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION && fenetre.getNomImage().getText().isEmpty() == false) {
				if (fenetre.getChoixJeu().getSelectedItem() == Jeu.timeline) {
					fileChooser.getSelectedFile().renameTo(
							new File("data/timeline/cards/" + fenetre.getTextNomImage().getText() + "_date.jpeg"));
				} else {
					fileChooser.getSelectedFile().renameTo(
							new File("data/cardline/cards/" + fenetre.getTextNomImage().getText() + "_reponse.jpeg"));
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!", "Informations",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	private class AjouterActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (fenetre.getChoixJeu().getSelectedItem() == Jeu.timeline) {
				if (fenetre.getTextInvention().getText().isEmpty() == true
						|| fenetre.getTextDateInvention().getText().isEmpty() == true
						|| fenetre.getTextNomImage().getText().isEmpty() == true) {
					JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs", "Informations",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					PrintStream l_out = null;
					try {
						l_out = new PrintStream(new FileOutputStream("data/timeline/timeline.csv", true));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
					int suite;
					try {
						Integer.parseInt(fenetre.getTextDateInvention().getText());
						suite = 1;
					} catch (Exception exep) {
						JOptionPane.showMessageDialog(null,
								"Veuillez remplir le champ date avec un nombre entier (-556 ou 1978) !!",
								"Informations", JOptionPane.INFORMATION_MESSAGE);
						suite = 0;
					}
					if (suite == 1) {
						String test = fenetre.getTextNomImage().getText();
						l_out.print(fenetre.getTextInvention().getText() + ";");
						l_out.print(fenetre.getTextDateInvention().getText() + ";");
						l_out.print(fenetre.getTextNomImage().getText() + "\n");
						l_out.flush();
						l_out.close();
						l_out = null;
						fenetre.dispose();
					}
				}
			} else {
				if (fenetre.getTextPays().getText().isEmpty() == true
						|| fenetre.getTextPIB().getText().isEmpty() == true
						|| fenetre.getTextPollution().getText().isEmpty() == true
						|| fenetre.getTextPopulation().getText().isEmpty() == true
						|| fenetre.getTextSuperficie().getText().isEmpty() == true
						|| fenetre.getTextNomImage().getText().isEmpty() == true) {
					JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs", "Informations",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					PrintStream l_out = null;
					try {
						l_out = new PrintStream(new FileOutputStream("data/cardline/cardline.csv", true));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
					int suite;
					try {
						Integer.parseInt(fenetre.getTextPIB().getText());
						suite = 1;
						Integer.parseInt(fenetre.getPopulation().getText());
						suite++;
						Double.parseDouble(fenetre.getPollution().getText());
						suite++;
						Double.parseDouble(fenetre.getSuperficie().getText());
						suite++;

					} catch (Exception exep) {
						JOptionPane.showMessageDialog(null,
								"Veuillez remplir le champ date avec un nombre entier (-556 ou 1978) !!",
								"Informations", JOptionPane.INFORMATION_MESSAGE);
						suite = 0;
					}
					if (suite == 4) {
						l_out.print(fenetre.getTextPays().getText() + ";");
						l_out.print(fenetre.getTextSuperficie().getText().replaceAll(".", ",") + ";");
						l_out.print(fenetre.getTextPopulation().getText() + ";");
						l_out.print(fenetre.getTextPIB().getText() + ";");
						l_out.print(fenetre.getTextPollution().getText().replaceAll(".", ",") + ";");
						l_out.print(fenetre.getTextNomImage().getText() + "\n");
						l_out.flush();
						l_out.close();
						l_out = null;
						fenetre.dispose();
					}
				}
			}
		}
	}
}
