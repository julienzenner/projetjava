package controleur;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.TransferHandler;

import modele.Carte;
import modele.Joueur;
import modele.Plateau;
import vue.FenetreCarteCachee;
import vue.FenetreCarteVisible;
import vue.PanelJeu;

public class ControleurPanelJeu {

	private Plateau plateau;
	private PanelJeu panelJeu;
	private ArrayList<JButton> boutonTapis;
	private ArrayList<JButton> boutonCarteTapis;
	private ArrayList<JButton> boutonMainJoueur;
	private Carte carteJoueurSelectionnee;
	private Timer timerPartie;
	private Timer timerAAfficher;
	private Timer timerAide;
	private int temps;

	public ControleurPanelJeu(Plateau plateau, PanelJeu panelJeu) {
		super();
		this.plateau = plateau;
		this.panelJeu = panelJeu;
		this.boutonTapis = new ArrayList<JButton>();
		this.setBoutonCarteTapis(new ArrayList<JButton>());
		this.boutonMainJoueur = new ArrayList<JButton>();
		this.timerAAfficher = new Timer(1000, new TimerAAfficherAction());
		this.timerPartie = new Timer(30000, new TimerPartieAction());
		this.temps = 30;
		this.timerAide = new Timer(15000, new TimerAideAction());
		carteSon("data/sounds/distributionCarte.wav");
		initialisationBoutonGaucheDroite();
		this.plateau.getFriseChronologique().forEach(carte -> {
			JButton button = new JButton(new ImageIcon(new ImageIcon(carte.getIllustrationVisible()).getImage()
					.getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
			button.setToolTipText(carte.getLibelle());
			button.addActionListener(new FenetreCarteTapisAction());
			this.panelJeu.getPanelTapis().add(button);
			this.boutonTapis.add(button);
			this.boutonCarteTapis.add(button);
			initialisationBoutonGaucheDroite();
		});
		initilisationMainJoueur(plateau.getNumJoueur());
		this.panelJeu.getJoueur().setText("C'est au tour de " + plateau.getJoueurs(plateau.getNumJoueur()));
		timerPartie.start();
		timerAAfficher.start();
		timerAide.start();
	}

	private void initilisationMainJoueur(int numJoueur) {
		this.plateau.getJoueurs(numJoueur).getMain().forEach(carte -> {
			JButton buttonMain = new JButton(new ImageIcon(new ImageIcon(carte.getIllustrationCachee()).getImage()
					.getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
			buttonMain.addMouseListener(new SourisHoverCarteJoueurAction());
			buttonMain.addActionListener(new ClicCarteMainJoueurAction());
			buttonMain.setToolTipText(carte.getLibelle());
			buttonMain.setTransferHandler(new TransferHandler("icon"));
			this.boutonMainJoueur.add(buttonMain);
			this.panelJeu.getPanelJoueurMain().add(buttonMain);
		});
	}

	private void initialisationBoutonGaucheDroite() {
		JButton button = new JButton("+");
		button.setPreferredSize(new Dimension(20, 20));
		button.addActionListener(new ClicCarteTapisAction());
		this.panelJeu.getPanelTapis().add(button);
		this.boutonTapis.add(button);
	}

	public void reorganisationTapis() {
		this.boutonTapis.clear();
		this.boutonCarteTapis.clear();
		initialisationBoutonGaucheDroite();
		this.plateau.getFriseChronologique().forEach(carte -> {
			JButton button = new JButton(new ImageIcon(new ImageIcon(carte.getIllustrationVisible()).getImage()
					.getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
			button.setToolTipText(carte.getLibelle());
			button.addActionListener(new FenetreCarteTapisAction());
			this.boutonTapis.add(button);
			initialisationBoutonGaucheDroite();
			this.boutonCarteTapis.add(button);
		});
		this.panelJeu.getPanelTapis().removeAll();
		this.boutonTapis.forEach(bouton -> this.panelJeu.getPanelTapis().add(bouton));
		this.panelJeu.getPanelTapis().revalidate();
		this.panelJeu.getPanelTapis().repaint();
	}

	private void carteSon(String path) {
		AudioInputStream audioInputStream = null;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(new File(path));
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public PanelJeu getPanelJeu() {
		return panelJeu;
	}

	public void setPanelJeu(PanelJeu panelJeu) {
		this.panelJeu = panelJeu;
	}

	public ArrayList<JButton> getBoutonTapis() {
		return boutonTapis;
	}

	public void setBoutonTapis(ArrayList<JButton> boutonTapis) {
		this.boutonTapis = boutonTapis;
	}

	public ArrayList<JButton> getBoutonMainJoueur() {
		return boutonMainJoueur;
	}

	public void setBoutonMainJoueur(ArrayList<JButton> boutonMainJoueur) {
		this.boutonMainJoueur = boutonMainJoueur;
	}

	public Carte getCarteJoueurSelectionnee() {
		return carteJoueurSelectionnee;
	}

	public void setCarteJoueurSelectionnee(Carte carteJoueurSelectionnee) {
		this.carteJoueurSelectionnee = carteJoueurSelectionnee;
	}

	public ArrayList<JButton> getBoutonCarteTapis() {
		return boutonCarteTapis;
	}

	public void setBoutonCarteTapis(ArrayList<JButton> boutonCarteTapis) {
		this.boutonCarteTapis = boutonCarteTapis;
	}

	public Timer getTimerPartie() {
		return timerPartie;
	}

	public void setTimerPartie(Timer timerPartie) {
		this.timerPartie = timerPartie;
	}

	public Timer getTimerAAfficher() {
		return timerAAfficher;
	}

	public void setTimerAAfficher(Timer timerAAfficher) {
		this.timerAAfficher = timerAAfficher;
	}

	public Timer getTimerAide() {
		return timerAide;
	}

	public void setTimerAide(Timer timerAide) {
		this.timerAide = timerAide;
	}

	public int getTemps() {
		return temps;
	}

	public void setTemps(int temps) {
		this.temps = temps;
	}

	// A LA FIN
	private class ClicCarteTapisAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (carteJoueurSelectionnee != null) {
				carteSon("data/sounds/carte.wav");
				int index = boutonTapis.indexOf(e.getSource());
				boutonTapis.remove(index);
				JButton button = new JButton(
						new ImageIcon(new ImageIcon(carteJoueurSelectionnee.getIllustrationVisible()).getImage()
								.getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
				button.addActionListener(new FenetreCarteTapisAction());
				boutonTapis.add(index, button);
				boutonCarteTapis.clear();
				boutonTapis.forEach(carte -> boutonCarteTapis.add(carte));
				boutonCarteTapis.removeIf(carte -> carte.getIcon() == null);
				int index2 = boutonCarteTapis.indexOf(button);
				plateau.addFriseChronologique(index2, carteJoueurSelectionnee);
				plateau.verificationFinDeTourJoueur(plateau.getJoueurs(plateau.getNumJoueur()),
						carteJoueurSelectionnee);
				if (plateau.getFriseChronologique().size() == boutonCarteTapis.size()) {
					JButton buttonGauche = new JButton("+");
					buttonGauche.setPreferredSize(new Dimension(20, 20));
					buttonGauche.addActionListener(new ClicCarteTapisAction());
					JButton buttonDroite = new JButton("+");
					buttonDroite.setPreferredSize(new Dimension(20, 20));
					buttonDroite.addActionListener(new ClicCarteTapisAction());
					boutonTapis.add(index, buttonGauche);
					boutonTapis.add(index + 2, buttonDroite);
				} else {
					carteSon("data/sounds/fail.wav");
					boutonTapis.remove(index);
					JButton buttonAjout = new JButton("+");
					buttonAjout.setPreferredSize(new Dimension(20, 20));
					buttonAjout.addActionListener(new ClicCarteTapisAction());
					boutonTapis.add(index, buttonAjout);
				}
				panelJeu.getPanelTapis().removeAll();
				boutonTapis.forEach(bouton -> panelJeu.getPanelTapis().add(bouton));
				panelJeu.getPanelTapis().revalidate();
				panelJeu.getPanelTapis().repaint();
				if (plateau.getNumJoueur() == plateau.getJoueurs().size() - 1) { // a tester
					plateau.verificationFinDeTour();
					if (plateau.getJoueursPerdant().size() > 0) {
						Joueur joueur = plateau.joueurGagnant();
						if (joueur != null) {
							JOptionPane.showConfirmDialog(null,"Le joueur " + joueur + " a gagné la partie !", "Aide", JOptionPane.YES_OPTION);
						}
					}
					plateau.setNumJoueur(0);
				} else {
					plateau.setNumJoueur(plateau.getNumJoueur() + 1);
				}
				panelJeu.getPanelJoueurMain().removeAll();
				boutonMainJoueur.clear();
				initilisationMainJoueur(plateau.getNumJoueur());
				panelJeu.getPanelJoueurMain().revalidate();
				panelJeu.getPanelJoueurMain().repaint();
				timerAAfficher.stop();
				timerPartie.stop();
				timerAide.stop();
				temps = 30;
				panelJeu.getTemps().setText("Temps restant : " + temps + " secondes");
				panelJeu.getJoueur().setText("C'est au tour de " + plateau.getJoueurs(plateau.getNumJoueur()));
				timerPartie.start();
				timerAAfficher.start();
				timerAide.start();
				carteJoueurSelectionnee = null;
			}
		}
	}

	private class ClicCarteMainJoueurAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			carteJoueurSelectionnee = plateau.getJoueurs(plateau.getNumJoueur())
					.getMain(boutonMainJoueur.indexOf((JButton) e.getSource()));
		}
	}

	private class SourisHoverCarteJoueurAction implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				new FenetreCarteCachee(carteJoueurSelectionnee);
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			((AbstractButton) e.getSource())
					.setIcon(new ImageIcon(new ImageIcon(plateau.getJoueurs(plateau.getNumJoueur())
							.getMain(boutonMainJoueur.indexOf((JButton) e.getSource())).getIllustrationCachee())
									.getImage().getScaledInstance(200, 220, Image.SCALE_DEFAULT)));
		}

		@Override
		public void mouseExited(MouseEvent e) {
			((AbstractButton) e.getSource())
					.setIcon(new ImageIcon(new ImageIcon(plateau.getJoueurs(plateau.getNumJoueur())
							.getMain(boutonMainJoueur.indexOf((JButton) e.getSource())).getIllustrationCachee())
									.getImage().getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
		}
	}

	private class FenetreCarteTapisAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new FenetreCarteVisible(plateau.getFriseChronologique(boutonCarteTapis.indexOf((JButton) e.getSource())));
		}
	}

	private class TimerPartieAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			temps = 30;
			if (plateau.getNumJoueur() == plateau.getJoueurs().size() - 1) {
				plateau.setNumJoueur(0);
			} else {
				plateau.setNumJoueur(plateau.getNumJoueur() + 1);
			}
			panelJeu.getPanelJoueurMain().removeAll();
			boutonMainJoueur.clear();
			initilisationMainJoueur(plateau.getNumJoueur());
			panelJeu.getJoueur().setText("C'est au tour de " + plateau.getJoueurs(plateau.getNumJoueur()));
			panelJeu.getPanelJoueurMain().revalidate();
			panelJeu.getPanelJoueurMain().repaint();
			timerAide.stop();
			timerAide.start();
		}
	}

	private class TimerAAfficherAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			temps = temps - 1;
			panelJeu.getTemps().setText("Temps restant : " + temps + " secondes");
		}
	}

	private class TimerAideAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (plateau.getJoueurs(plateau.getNumJoueur()).isJoker()) {
				int option = JOptionPane.showConfirmDialog(null,
						"Voulez-vous utiliser une Aide ? Cette aide est utilisable qu'une fois par partie. Elle permet de placer au bon endroit une carte de votre main sur le tapis",
						"Aide", JOptionPane.YES_NO_OPTION);
				if (option == JOptionPane.YES_OPTION) {
					carteSon("data/sounds/carte.wav");
					plateau.getJoueurs(plateau.getNumJoueur()).setJoker(false);
					int posCarte = (int) (Math.random() * plateau.getJoueurs(plateau.getNumJoueur()).getMain().size());
					boolean trouve = false;
					int indexCarte = 0;
					while (trouve == false) {
						plateau.addFriseChronologique(indexCarte,
								plateau.getJoueurs(plateau.getNumJoueur()).getMain(posCarte));
						if (plateau.verificationPlacement(plateau.getJoueurs(plateau.getNumJoueur()),
								plateau.getJoueurs(plateau.getNumJoueur()).getMain(posCarte))) {
							trouve = true;
							plateau.verificationFinDeTourJoueur(plateau.getJoueurs(plateau.getNumJoueur()),
									plateau.getJoueurs(plateau.getNumJoueur()).getMain(posCarte));
						} else {
							plateau.removeFriseChronologique(
									plateau.getJoueurs(plateau.getNumJoueur()).getMain(posCarte));
							indexCarte++;
						}
					}
					JButton button = new JButton(new ImageIcon(
							new ImageIcon(plateau.getFriseChronologique(indexCarte).getIllustrationVisible()).getImage()
									.getScaledInstance(160, 200, Image.SCALE_DEFAULT)));
					button.addActionListener(new FenetreCarteTapisAction());
					boutonCarteTapis.add(indexCarte, button);
					boutonTapis.clear();
					initialisationBoutonGaucheDroite();
					boutonCarteTapis.forEach(carte -> {
						boutonTapis.add(carte);
						initialisationBoutonGaucheDroite();
					});
					panelJeu.getPanelTapis().removeAll();
					boutonTapis.forEach(bouton -> panelJeu.getPanelTapis().add(bouton));
					panelJeu.getPanelTapis().revalidate();
					panelJeu.getPanelTapis().repaint();
					if (plateau.getNumJoueur() == plateau.getJoueurs().size() - 1) {
						plateau.verificationFinDeTour();
						if (plateau.getJoueursPerdant().size() > 0) {
							plateau.joueurGagnant();
						}
						plateau.setNumJoueur(0);
					} else {
						plateau.setNumJoueur(plateau.getNumJoueur() + 1);
					}
					panelJeu.getPanelJoueurMain().removeAll();
					boutonMainJoueur.clear();
					initilisationMainJoueur(plateau.getNumJoueur());
					panelJeu.getPanelJoueurMain().revalidate();
					panelJeu.getPanelJoueurMain().repaint();
					timerAAfficher.stop();
					timerPartie.stop();
					timerAide.stop();
					temps = 30;
					panelJeu.getTemps().setText("Temps restant " + temps + " secondes");
					panelJeu.getJoueur().setText("C'est au tour de " + plateau.getJoueurs(plateau.getNumJoueur()));
					timerPartie.start();
					timerAAfficher.start();
					timerAide.start();
					carteJoueurSelectionnee = null;
				}
			}
		}
	}
}