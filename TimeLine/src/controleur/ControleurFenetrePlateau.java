package controleur;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import modele.Plateau;
import modele.enumJeu.Jeu;
import vue.FenetreChangerTheme;
import vue.FenetreGestion;
import vue.FenetrePlateau;
import vue.FenetreVoirScore;

public class ControleurFenetrePlateau {
	private Plateau plateau;
	private FenetrePlateau fenetre;
	private FenetreGestion fenetreGestion;
	private ControleurPanelJeu controleurPanelJeu;

	public ControleurFenetrePlateau(Plateau plateau, FenetrePlateau fenetre, FenetreGestion fenetreGestion) {
		this.plateau = plateau;
		this.fenetre = fenetre;
		this.fenetreGestion = fenetreGestion;
		this.fenetre.setTitle(plateau.getTypeJeu().toString());
		this.controleurPanelJeu = new ControleurPanelJeu(plateau, this.fenetre.getPanelJeu());
		this.fenetre.getMenuEnregistrer().getMenuItemEnregistrer().addActionListener(new MenuItemEnregistrerAction());
		this.fenetre.getMenuEnregistrer().getMenuAide().addMenuListener(new MenuAideAction());
		this.fenetre.getMenuEnregistrer().getMenuItemTheme().addActionListener(new MenuItemThemeAction());
		this.fenetre.getMenuEnregistrer().getMenuItemVoirScore().addActionListener(new MenuItemVoirScoreAction());
		this.fenetre.addWindowListener(new WindowCloseAction());
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public FenetrePlateau getFenetre() {
		return fenetre;
	}

	public void setFenetre(FenetrePlateau fenetre) {
		this.fenetre = fenetre;
	}

	public ControleurPanelJeu getControleurPanelJeu() {
		return controleurPanelJeu;
	}

	public void setControleurPanelJeu(ControleurPanelJeu controleurPanelJeu) {
		this.controleurPanelJeu = controleurPanelJeu;
	}

	private class MenuItemEnregistrerAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			if (fileChooser.showSaveDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
				try {
					FileOutputStream f = null;
					if (fileChooser.getSelectedFile().exists()) {
						f = new FileOutputStream(fileChooser.getSelectedFile());
					} else {
						f = new FileOutputStream(fileChooser.getSelectedFile() + ".jeu");
					}
					ObjectOutputStream s = new ObjectOutputStream(f);
					s.writeObject(plateau);
					s.flush();
					s.close();
					f.close();
				} catch (Exception exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private class MenuAideAction implements MenuListener {

		@Override
		public void menuSelected(MenuEvent e) {
			try {
				Desktop.getDesktop().open(new File("data/regle.pdf"));
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}

		@Override
		public void menuDeselected(MenuEvent e) {
		}

		@Override
		public void menuCanceled(MenuEvent e) {
		}
	}

	private class MenuItemThemeAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (plateau.getTypeJeu() == Jeu.cardline) {
				new ControleurFenetreChangerTheme(plateau, new FenetreChangerTheme(), controleurPanelJeu);
			}
		}
	}

	private class MenuItemVoirScoreAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new FenetreVoirScore(plateau);
		}
	}

	private class WindowCloseAction implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
		}

		@Override
		public void windowClosing(WindowEvent e) {
			controleurPanelJeu.getTimerAAfficher().stop();
			controleurPanelJeu.getTimerAide().stop();
			controleurPanelJeu.getTimerPartie().stop();
			fenetreGestion.setVisible(true);
			plateau.getJoueurs().addAll(plateau.getJoueursPerdant());
			plateau.getJoueurs().forEach(joueur -> joueur.getMain().clear());
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}
	}
}