package modele;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

public class Plateau implements Serializable {

	private static final long serialVersionUID = 8756853199246437895L;
	private List<Carte> paquet;
	private List<Carte> defausse;
	private List<Carte> friseChronologique;
	private List<Joueur> joueurs;
	private List<Joueur> joueursPerdant;
	private Jeu typeJeu;
	private Integer numJoueur;

	public Plateau(List<Carte> paquet, List<Carte> defausse, List<Carte> friseChronologique, List<Joueur> joueurs,
			List<Joueur> joueursPerdant, Jeu typeJeu, Integer numJoueur) {
		this.paquet = paquet;
		this.defausse = defausse;
		this.friseChronologique = friseChronologique;
		this.joueurs = joueurs;
		this.joueursPerdant = joueursPerdant;
		this.typeJeu = typeJeu;
		this.numJoueur = numJoueur;
	}

	public Plateau() {
		this.paquet = new ArrayList<Carte>();
		this.defausse = new ArrayList<Carte>();
		this.friseChronologique = new ArrayList<Carte>();
		this.joueurs = new ArrayList<Joueur>();
		this.joueursPerdant = new ArrayList<Joueur>();
		this.numJoueur = 0;
	}

	public Plateau(Plateau readObject) {
		this.paquet = readObject.getPaquet();
		this.defausse = readObject.getDefausse();
		this.friseChronologique = readObject.getFriseChronologique();
		this.joueurs = readObject.getJoueurs();
		this.joueursPerdant = readObject.getJoueursPerdant();
		this.typeJeu = readObject.getTypeJeu();
		this.numJoueur = readObject.getNumJoueur();
	}

	public Integer getNumJoueur() {
		return numJoueur;
	}

	public void setNumJoueur(Integer numJoueur) {
		this.numJoueur = numJoueur;
	}

	public List<Carte> getPaquet() {
		return paquet;
	}

	public Carte getPaquet(int index) {
		return paquet.get(index);
	}

	public void setPaquet(List<Carte> paquet) {
		this.paquet = paquet;
	}

	public boolean addPaquet(Carte carte) {
		if (this.paquet.add(carte)) {
			return true;
		}
		return false;
	}

	public boolean removePaquet(Carte carte) {
		if (this.paquet.remove(carte)) {
			return true;
		}
		return false;
	}

	public List<Carte> getDefausse() {
		return defausse;
	}

	public Carte getDefausse(int index) {
		return defausse.get(index);
	}

	public void setDefausse(List<Carte> defausse) {
		this.defausse = defausse;
	}

	public boolean addDefausse(Carte carte) {
		if (this.defausse.add(carte)) {
			return true;
		}
		return false;
	}

	public boolean removeDefausse(Carte carte) {
		if (this.defausse.remove(carte)) {
			return true;
		}
		return false;
	}

	public List<Carte> getFriseChronologique() {
		return friseChronologique;
	}

	public Carte getFriseChronologique(int index) {
		return friseChronologique.get(index);
	}

	public void setFriseChronologique(List<Carte> friseChronologique) {
		this.friseChronologique = friseChronologique;
	}

	public boolean addFriseChronologique(Carte carte) {
		if (this.friseChronologique.add(carte)) {
			return true;
		}
		return false;
	}

	public void addFriseChronologique(int index, Carte carte) {
		this.friseChronologique.add(index, carte);
	}

	public boolean removeFriseChronologique(Carte carte) {
		if (this.friseChronologique.remove(carte)) {
			return true;
		}
		return false;
	}

	/**
	 * Tri la liste joueur en fonction de leurs date de naissance. Utile pour savoir
	 * qui commence.
	 */
	public void triJoueurs() {
		Collections.sort(this.joueurs,
				(joueur1, joueur2) -> joueur2.getDateNaissance().compareTo(joueur1.getDateNaissance()));
	}

	public void triTheme() {
		Collections.sort(this.friseChronologique, (carte1, carte2) -> carte1.getTheme().compareTo(carte2.getTheme()));
	}

	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	public Joueur getJoueurs(int index) {
		return joueurs.get(index);
	}

	public void setJoueurs(List<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public boolean addJoueurs(Joueur joueur) {
		if (this.joueurs.add(joueur)) {
			return true;
		}
		return false;
	}

	public boolean removeJoueurs(Joueur joueur) {
		if (this.joueurs.remove(joueur)) {
			return true;
		}
		return false;
	}

	public List<Joueur> getJoueursPerdant() {
		return joueursPerdant;
	}

	public void setJoueursPerdant(List<Joueur> joueursPerdant) {
		this.joueursPerdant = joueursPerdant;
	}

	public boolean addJoueursPerdant(Joueur joueur) {
		if (this.joueursPerdant.add(joueur)) {
			return true;
		}
		return false;
	}

	public boolean removeJoueursPerdant(Joueur joueur) {
		if (this.joueursPerdant.remove(joueur)) {
			return true;
		}
		return false;
	}

	/**
	 * Fonction de distribution des cartes en fonction du nombre de joueurs présent
	 * 
	 * @param plateau
	 */
	public void distribuerCarte() {
		int carteADistribue = 0;
		switch (this.joueurs.size()) {
		case 2:
		case 3:
			carteADistribue = 6;
			break;
		case 4:
		case 5:
			carteADistribue = 5;
			break;
		default:
			carteADistribue = 4;
		}
		for (int i = 0; i < carteADistribue; i++) {
			this.joueurs.forEach(joueur -> {
				Carte carte = carteAleatoire();
				joueur.addMain(carte);
				this.removePaquet(carte);
			});
		}
	}

	/**
	 * Retourne une carte aléatoire du paquet
	 * 
	 * @param plateau
	 * @return {@link Carte}
	 */
	private Carte carteAleatoire() {
		return this.getPaquet((int) (Math.random() * (this.getPaquet().size())));
	}

	/**
	 * Fonction d'initialisation de partie<br>
	 * Ajout d'une carte à la frise
	 * 
	 * @param plateau
	 */
	public void debutPartie() {
		Carte carte = carteAleatoire();
		this.addFriseChronologique(carte);
		this.removePaquet(carte);
	}

	/**
	 * Vérifie si la carte précedemment placée, se situe à la bonne place
	 * <ul>
	 * Pseudo-code :
	 * <li>Si carte bien placée --> aucune traitement</li>
	 * <li>Si carte mal placée --> défausse de la carte, ajout d'une nouvelle carte
	 * dans la main du joueur</li>
	 * </ul>
	 * 
	 * @param joueur
	 * @param plateau
	 * @param carte
	 */
	public boolean verificationPlacement(Joueur joueur, Carte carte) {
		int positionCarte = this.getFriseChronologique().indexOf(carte);
		Carte carteGauche = null, carteDroite = null;
		int resultatComparaisonCarteDroite = 0, resultatComparaisonCarteGauche = 0;
		if (positionCarte == 0) {
			carteDroite = this.getFriseChronologique(positionCarte + 1);
		} else if (positionCarte == this.getFriseChronologique().size() - 1) {
			carteGauche = this.getFriseChronologique(positionCarte - 1);
		} else {
			carteGauche = this.getFriseChronologique(positionCarte - 1);
			carteDroite = this.getFriseChronologique(positionCarte + 1);
		}
		boolean trie = false;
		if (carteGauche == null) {
			resultatComparaisonCarteDroite = carte.getTheme().compareTo(carteDroite.getTheme());
			if (resultatComparaisonCarteDroite < 0) {
				trie = true;
			}
		} else if (carteDroite == null) {
			resultatComparaisonCarteGauche = carteGauche.getTheme().compareTo(carte.getTheme());
			if (resultatComparaisonCarteGauche < 0) {
				trie = true;
			}
		} else {
			resultatComparaisonCarteDroite = carte.getTheme().compareTo(carteDroite.getTheme());
			resultatComparaisonCarteGauche = carteGauche.getTheme().compareTo(carte.getTheme());
			if (resultatComparaisonCarteDroite + resultatComparaisonCarteGauche < 0) {
				trie = true;
			}
		}
		return trie;
	}

	public void verificationFinDeTourJoueur(Joueur joueur, Carte carte) {
		if (!verificationPlacement(joueur, carte)) {
			this.addDefausse(carte);
			this.removeFriseChronologique(carte);
			Carte carteADistribue = carteAleatoire();
			joueur.addMain(carteADistribue);
			this.removePaquet(carteADistribue);
		}
		joueur.removeMain(carte);
	}

	/**
	 * Fonction de fin de tour
	 * <ul>
	 * Pseudo-code :
	 * <li>Si tous les joueurs ont tous plus de 0 cartes --> aucun traitement</li>
	 * <li>Si plusieurs joueurs ont 0 cartes --> joeurs encore en jeu, les autres
	 * sont éléminés</li>
	 * <li>Si un seul joueur a 0 carte --> gagnant</li>
	 * </ul>
	 * 
	 * @param plateau
	 */
	public void verificationFinDeTour() {
		List<Joueur> joueurRestant = new ArrayList<Joueur>();
		for (Joueur joueur : this.getJoueurs()) {
			if (joueur.getMain().size() == 0) {
				joueurRestant.add(joueur);
			}
		}
		if (joueurRestant.size() > 0) {
			for (Joueur joueur : this.getJoueurs()) {
				if (joueur.getMain().size() > 0) {
					this.getDefausse().addAll(joueur.getMain());
					this.addJoueursPerdant(joueur);
				}
			}
			if (joueurRestant.size() > 1) {
				for (Joueur joueur : joueurRestant) {
					Carte carte = carteAleatoire();
					joueur.addMain(carte);
					this.removePaquet(carte);
				}
			}
			this.setJoueurs(joueurRestant);
		}
	}

	/**
	 * Fonction déterminant le joueur gagnant
	 * 
	 * @param plateau
	 * @return {@link Joueur}
	 */
	public Joueur joueurGagnant() {
		if (this.joueurs.size() == 1) {
			this.getJoueurs(0).setScore(this.getJoueurs(0).getScore() + 1);
			return this.getJoueurs(0);
		}
		return null;
	}

	/**
	 * Charge un fichier CSV spécifique en fonction du Jeu de carte choisie
	 * 
	 * @param jeu
	 * @param jeuLib
	 * @param theme
	 * @throws IOException
	 */
	public void chargerCarte(ThemeCarte theme) throws IOException {
		File file = new File("data/" + this.typeJeu + "/" + this.typeJeu + ".csv");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		StringTokenizer st = null;
		String line = br.readLine();
		Double themeAComparer = 0.0;
		for (line = br.readLine(); line != null; line = br.readLine()) {
			st = new StringTokenizer(line, ";");
			String libelle = st.nextToken();
			if (Jeu.timeline.equals(this.typeJeu)) {
				Integer date = Integer.parseInt(st.nextToken());
				themeAComparer = (double) date;
				String url = st.nextToken();
				String imageCache = "data/" + this.typeJeu + "/cards/" + url + ".jpeg";
				String image = "data/" + this.typeJeu + "/cards/" + url + "_date" + ".jpeg";
				CarteTimeline carte = new CarteTimeline(libelle, date, imageCache, image, themeAComparer);
				this.paquet.add(carte);
			} else {
				Double superficie = Double.parseDouble(st.nextToken());
				Integer population = Integer.parseInt(st.nextToken());
				Integer pib = Integer.parseInt(st.nextToken());
				String pollutionCSV = st.nextToken();
				String pollutionClean = pollutionCSV.replace(',', '.');
				Double pollution = Double.parseDouble(pollutionClean);
				String url = st.nextToken();
				String imageCache = "data/" + this.typeJeu + "/cards/" + url + ".jpeg";
				String image = "data/" + this.typeJeu + "/cards/" + url + "_reponse" + ".jpeg";
				switch (theme) {
				case pib:
					themeAComparer = (double) pib;
					break;
				case pollution:
					themeAComparer = pollution;
					break;
				case population:
					themeAComparer = (double) population;
					break;
				case superficie:
					themeAComparer = superficie;
					break;
				}
				CarteCardline carte = new CarteCardline(libelle, superficie, population, pib, pollution, theme,
						imageCache, image, themeAComparer);
				this.paquet.add(carte);
			}
		}
		br.close();
	}

	public void changerTheme(ThemeCarte theme) {
		switch (theme) {
		case pib:
			this.paquet.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPib()));
			this.defausse.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPib()));
			this.friseChronologique.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPib()));
			triTheme();
			for (Joueur joueur : this.getJoueurs()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPib()));
			}
			for (Joueur joueur : this.getJoueursPerdant()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPib()));
			}
			break;
		case pollution:
			this.paquet.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPollution()));
			this.defausse.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPollution()));
			this.friseChronologique.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPollution()));
			triTheme();
			for (Joueur joueur : this.getJoueurs()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPollution()));
			}
			for (Joueur joueur : this.getJoueursPerdant()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPollution()));
			}
			break;
		case population:
			this.paquet.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPopulation()));
			this.defausse.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPopulation()));
			this.friseChronologique.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPopulation()));
			triTheme();
			for (Joueur joueur : this.getJoueurs()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPopulation()));
			}
			for (Joueur joueur : this.getJoueursPerdant()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getPopulation()));
			}
			break;
		case superficie:
			this.paquet.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getSuperficie()));
			this.defausse.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getSuperficie()));
			this.friseChronologique.forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getSuperficie()));
			triTheme();
			for (Joueur joueur : this.getJoueurs()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getSuperficie()));
			}
			for (Joueur joueur : this.getJoueursPerdant()) {
				joueur.getMain().forEach(carte -> carte.setTheme((double) ((CarteCardline) carte).getSuperficie()));
			}
			break;
		}
	}

	public Jeu getTypeJeu() {
		return typeJeu;
	}

	public void setTypeJeu(Jeu typeJeu) {
		this.typeJeu = typeJeu;
	}

	public void sauvegarderScores() throws IOException {
		File f = new File("data/scores/" + UUID.randomUUID() + ".csv");
		FileWriter fileWriter = null;
		String fileHeader = "pseudo,date naissance,score";
		String commaDelimiter = ",";
		String lineSeparator = "\n";
		try {
			f.createNewFile();
			fileWriter = new FileWriter(f);
			fileWriter.append(fileHeader);
			fileWriter.append(lineSeparator);
			for (Joueur joueur : this.getJoueurs()) {
				fileWriter.append(joueur.getPseudo());
				fileWriter.append(commaDelimiter);
				fileWriter.append(joueur.getDateNaissance().toString());
				fileWriter.append(commaDelimiter);
				fileWriter.append(joueur.getScore().toString());
				fileWriter.append(lineSeparator);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}