package modele;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ModelJTableVoirScore extends AbstractTableModel {

	private static final long serialVersionUID = 7792558035773949753L;
	private final List<JoueurScore> joueurs = new ArrayList<JoueurScore>();
	private final String[] entetes = { "Pseudo", "Date de naissance", "Score" };

	public ModelJTableVoirScore(Plateau plateau) {
		plateau.getJoueurs().forEach(joueur -> {
			this.joueurs.add(new JoueurScore(joueur.getPseudo(), joueur.getDateNaissance(), joueur.getScore()));
		});
		Collections.sort(this.joueurs, (joueur1, joueur2) -> joueur1.getScore().compareTo(joueur2.getScore()));
	}

	@Override
	public int getRowCount() {
		return joueurs.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
	public String getColumnName(int column) {
		return entetes[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return joueurs.get(rowIndex).getPseudo();
		case 1:
			return joueurs.get(rowIndex).getDateNaissance();
		case 2:
			return joueurs.get(rowIndex).getScore();
		default:
			return null;
		}
	}
}