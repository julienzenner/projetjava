package modele.enumJeu;

import java.io.Serializable;

public enum Jeu implements Serializable {
	cardline("cardline"),
	timeline("timeline");

	private String stateName;

	Jeu(String newStateName) {
		this.stateName = newStateName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}