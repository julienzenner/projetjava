package modele.enumJeu;

import java.io.Serializable;

public enum ThemeCarte implements Serializable {
	superficie("superficie"),
	population("population"),
	pib("pib"),
	pollution("pollution");

	private String stateName;

	ThemeCarte(final String newStateName) {
		this.stateName = newStateName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}
