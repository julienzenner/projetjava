package modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Joueur implements Serializable {

	private static final long serialVersionUID = 5917327370591874655L;
	private String pseudo;
	private Date dateNaissance;
	private List<Carte> main;
	private boolean joker;
	private Integer score;

	public Joueur(String pseudo, Date dateNaissance, List<Carte> main, boolean joker, Integer score) {
		this.pseudo = pseudo;
		this.dateNaissance = dateNaissance;
		this.main = main;
		this.joker = joker;
		this.score = score;
	}

	public Joueur(String pseudo, Date anneeNaissance) {
		this.pseudo = pseudo;
		this.dateNaissance = anneeNaissance;
		this.main = new ArrayList<Carte>();
		this.joker = true;
		this.score = 0;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public List<Carte> getMain() {
		return main;
	}

	public Carte getMain(int index) {
		return main.get(index);
	}

	public void setMain(List<Carte> main) {
		this.main = main;
	}

	public boolean addMain(Carte carte) {
		if (this.main.add(carte)) {
			return true;
		}
		return false;
	}

	public boolean removeMain(Carte carte) {
		if (this.main.remove(carte)) {
			return true;
		}
		return false;
	}

	public boolean isJoker() {
		return joker;
	}

	public void setJoker(boolean joker) {
		this.joker = joker;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateNaissance == null) ? 0 : dateNaissance.hashCode());
		result = prime * result + ((main == null) ? 0 : main.hashCode());
		result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (dateNaissance == null) {
			if (other.dateNaissance != null)
				return false;
		} else if (!dateNaissance.equals(other.dateNaissance))
			return false;
		if (main == null) {
			if (other.main != null)
				return false;
		} else if (!main.equals(other.main))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.pseudo;
	}
}