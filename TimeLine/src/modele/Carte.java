package modele;

import java.io.Serializable;

/**
 * Classe abstraite Carte, regroupe les attributs commun à tous les jeux de
 * carte
 */
public abstract class Carte implements Serializable {

	private static final long serialVersionUID = 140589123080068277L;
	private String libelle;
	private String illustrationCachee;
	private String illustrationVisible;
	private Double theme;

	public Carte(String libelle, String illustrationCachee, String illustrationVisible, Double theme) {
		this.libelle = libelle;
		this.illustrationCachee = illustrationCachee;
		this.illustrationVisible = illustrationVisible;
		this.theme = theme;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setInvention(String libelle) {
		this.libelle = libelle;
	}

	public String getIllustrationCachee() {
		return illustrationCachee;
	}

	public void setIllustrationCachee(String illustrationCachee) {
		this.illustrationCachee = illustrationCachee;
	}

	public String getIllustrationVisible() {
		return illustrationVisible;
	}

	public void setIllustrationVisible(String illustrationVisible) {
		this.illustrationVisible = illustrationVisible;
	}

	public Double getTheme() {
		return theme;
	}

	public void setTheme(Double theme) {
		this.theme = theme;
	}
}