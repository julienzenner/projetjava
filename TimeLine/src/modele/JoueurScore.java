package modele;

import java.util.Date;

public class JoueurScore {

	private String pseudo;
	private Date dateNaissance;
	private Integer score;

	public JoueurScore(String pseudo, Date dateNaissance, Integer score) {
		super();
		this.pseudo = pseudo;
		this.dateNaissance = dateNaissance;
		this.score = score;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
}