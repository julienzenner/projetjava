package modele;

public class CarteTimeline extends Carte {

	private static final long serialVersionUID = -8097540058712211008L;
	private Integer date;

	public CarteTimeline(String libelle, Integer date, String illustrationCachee, String illustrationVisible,
			Double theme) {
		super(libelle, illustrationCachee, illustrationVisible, theme);
		this.date = date;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}
}