package modele;

import modele.enumJeu.ThemeCarte;

public class CarteCardline extends Carte {

	private static final long serialVersionUID = -6572774982781397180L;
	private Double superficie;
	private Integer population;
	private Integer pib;
	private Double pollution;
	private ThemeCarte themeCarte;

	public CarteCardline(String libelle, Double superficie, Integer population, Integer pib, Double pollution,
			ThemeCarte themeCarte, String illustrationCachee, String illustrationVisible, Double theme) {
		super(libelle, illustrationCachee, illustrationVisible, theme);
		this.superficie = superficie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
		this.setThemeCarte(themeCarte);
	}

	public Double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(Double superficie) {
		this.superficie = superficie;
	}

	public Integer getPopulation() {
		return population;
	}

	public void setPopulation(Integer population) {
		this.population = population;
	}

	public Integer getPib() {
		return pib;
	}

	public void setPib(Integer pib) {
		this.pib = pib;
	}

	public Double getPollution() {
		return pollution;
	}

	public void setPollution(Double pollution) {
		this.pollution = pollution;
	}

	public ThemeCarte getThemeCarte() {
		return themeCarte;
	}

	public void setThemeCarte(ThemeCarte themeCarte) {
		this.themeCarte = themeCarte;
	}
}