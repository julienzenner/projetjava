package vue;

import java.awt.Font;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuEnregistrer extends JMenuBar {
	private static final long serialVersionUID = -2646606467969487514L;
	private JMenu menuFichier;
	private JMenuItem menuItemEnregistrer;
	private JMenu menuAide;
	private JMenu menuTheme;
	private JMenuItem menuItemTheme;
	private JMenu menuScore;
	private JMenuItem menuItemVoirScore;
	private Font police;

	public MenuEnregistrer() {
		this.police = new Font("Serif", Font.BOLD, 14);
		this.setFont(police);
		this.menuFichier = new JMenu("Fichier");
		this.menuItemEnregistrer = new JMenuItem("Enregister-sous");
		this.menuAide = new JMenu("Aide");
		this.menuTheme = new JMenu("Theme");
		this.menuItemTheme = new JMenuItem("Changer de Thème");
		this.menuScore = new JMenu("Score");
		this.menuItemVoirScore = new JMenuItem("Voir les scores");
		this.add(menuFichier);
		this.menuItemEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.META_DOWN_MASK));
		this.menuFichier.add(menuItemEnregistrer);
		this.add(menuFichier);
		this.add(menuAide);
		this.menuTheme.add(menuItemTheme);
		this.add(menuTheme);
		this.menuScore.add(menuItemVoirScore);
		this.add(menuScore);
	}

	public JMenu getMenuFichier() {
		return menuFichier;
	}

	public void setMenuFichier(JMenu menuFichier) {
		this.menuFichier = menuFichier;
	}

	public JMenuItem getMenuItemEnregistrer() {
		return menuItemEnregistrer;
	}

	public void setMenuItemEnregistrer(JMenuItem menuItemEnregistrer) {
		this.menuItemEnregistrer = menuItemEnregistrer;
	}

	public JMenu getMenuAide() {
		return menuAide;
	}

	public void setMenuAide(JMenu menuAide) {
		this.menuAide = menuAide;
	}

	public JMenu getMenuTheme() {
		return menuTheme;
	}

	public void setMenuTheme(JMenu menuTheme) {
		this.menuTheme = menuTheme;
	}

	public JMenuItem getMenuItemTheme() {
		return menuItemTheme;
	}

	public void setMenuItemTheme(JMenuItem menuItemTheme) {
		this.menuItemTheme = menuItemTheme;
	}

	public JMenu getMenuScore() {
		return menuScore;
	}

	public void setMenuScore(JMenu menuScore) {
		this.menuScore = menuScore;
	}

	public JMenuItem getMenuItemVoirScore() {
		return menuItemVoirScore;
	}

	public void setMenuItemVoirScore(JMenuItem menuItemVoirScore) {
		this.menuItemVoirScore = menuItemVoirScore;
	}
}