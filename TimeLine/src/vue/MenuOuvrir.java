package vue;

import java.awt.Font;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuOuvrir extends JMenuBar {
	private static final long serialVersionUID = -2646606467969487514L;
	private JMenu menuFichier;
	private JMenuItem menuItemOuvrir;
	private JMenu menuAide;
	private JMenu menuAjoutCarte;
	private JMenuItem menuItemAjoutCarte;
	private Font police;

	public MenuOuvrir() {
		this.police = new Font("Serif", Font.BOLD, 14);
		this.setFont(police);
		this.menuFichier = new JMenu("Fichier");
		this.menuItemOuvrir = new JMenuItem("Ouvrir");
		this.menuAide = new JMenu("Aide");
		this.menuAjoutCarte = new JMenu("Ajout de Cartes");
		this.menuItemAjoutCarte = new JMenuItem("Ajouter cartes");
		this.add(menuFichier);
		this.menuItemOuvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.META_DOWN_MASK));
		this.menuFichier.add(menuItemOuvrir);
		this.add(menuFichier);
		this.add(menuAide);
		this.menuAjoutCarte.add(menuItemAjoutCarte);
		this.add(menuAjoutCarte);
	}

	public JMenu getMenuAide() {
		return menuAide;
	}

	public void setMenuAide(JMenu menuAide) {
		this.menuAide = menuAide;
	}

	public JMenu getMenuFichier() {
		return menuFichier;
	}

	public void setMenuFichier(JMenu menuFichier) {
		this.menuFichier = menuFichier;
	}

	public JMenuItem getMenuItemOuvrir() {
		return menuItemOuvrir;
	}

	public void setMenuItemOuvrir(JMenuItem menuItemOuvrir) {
		this.menuItemOuvrir = menuItemOuvrir;
	}

	public JMenu getMenuAjoutCarte() {
		return menuAjoutCarte;
	}

	public void setMenuAjoutCarte(JMenu menuAjoutCarte) {
		this.menuAjoutCarte = menuAjoutCarte;
	}

	public JMenuItem getMenuItemAjoutCarte() {
		return menuItemAjoutCarte;
	}

	public void setMenuItemAjoutCarte(JMenuItem menuItemAjoutCarte) {
		this.menuItemAjoutCarte = menuItemAjoutCarte;
	}
}