package vue;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.enumJeu.ThemeCarte;

public class FenetreChangerTheme extends JFrame {

	private static final long serialVersionUID = 4003107127720593261L;
	private JLabel theme;
	private JComboBox<ThemeCarte> comboTheme;
	private JButton ok;
	private JPanel panelTheme;
	private JPanel panelBouton;

	public FenetreChangerTheme() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Changer de Thème");
		this.theme = new JLabel("Quel thème choisir ?");
		this.comboTheme = new JComboBox<ThemeCarte>();
		this.comboTheme.addItem(ThemeCarte.pib);
		this.comboTheme.addItem(ThemeCarte.pollution);
		this.comboTheme.addItem(ThemeCarte.population);
		this.comboTheme.addItem(ThemeCarte.superficie);
		this.ok = new JButton("Ok");
		this.panelTheme = new JPanel(new GridLayout(1, 1));
		this.panelBouton = new JPanel();
		this.getContentPane().setLayout(new BorderLayout());
		this.panelTheme.add(theme);
		this.panelTheme.add(comboTheme);
		this.panelBouton.add(ok);

		this.add(panelTheme, BorderLayout.CENTER);
		this.add(panelBouton, BorderLayout.SOUTH);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public JLabel getTheme() {
		return theme;
	}

	public void setTheme(JLabel theme) {
		this.theme = theme;
	}

	public JComboBox<ThemeCarte> getComboTheme() {
		return comboTheme;
	}

	public void setComboTheme(JComboBox<ThemeCarte> comboTheme) {
		this.comboTheme = comboTheme;
	}

	public JButton getOk() {
		return ok;
	}

	public void setOk(JButton ok) {
		this.ok = ok;
	}

	public JPanel getPanelTheme() {
		return panelTheme;
	}

	public void setPanelTheme(JPanel panelTheme) {
		this.panelTheme = panelTheme;
	}

	public JPanel getPanelBouton() {
		return panelBouton;
	}

	public void setPanelBouton(JPanel panelBouton) {
		this.panelBouton = panelBouton;
	}
}
