package vue;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class PanelJeu extends JPanel {
	private static final long serialVersionUID = -5644206738024784421L;
	private JPanel panelJoueurMain;
	private JPanel panelTapis;
	private JPanel panelInformations;
	private JLabel joueur;
	private JLabel joueurRestant;
	private JLabel temps;
	private JScrollPane panelScrollTapis;
	private JScrollPane panelScrollJoueurMain;
	private Font police;

	public PanelJeu() {
		this.police = new Font("Serif", Font.BOLD, 16);
		this.setLayout(new BorderLayout());
		this.panelJoueurMain = new JPanel();
		this.panelTapis = new JPanel(new WrapLayout());
		this.panelInformations = new JPanel();
		this.panelScrollTapis = new JScrollPane(panelTapis);
		this.panelScrollJoueurMain = new JScrollPane(panelJoueurMain);
		this.add(panelScrollTapis, BorderLayout.CENTER);
		this.add(panelScrollJoueurMain, BorderLayout.SOUTH);
		this.add(panelInformations, BorderLayout.NORTH);
		this.panelInformations.setLayout(new BorderLayout());
		this.joueur = new JLabel("C'est au tour de ");
		this.joueur.setFont(police);
		this.temps = new JLabel("Temps restant : 30 secondes");
		this.temps.setFont(police);
		this.panelInformations.add(joueur, BorderLayout.WEST);
		this.panelInformations.add(temps, BorderLayout.EAST);
	}

	public JPanel getPanelJoueurMain() {
		return panelJoueurMain;
	}

	public void setPanelJoueurMain(JPanel panelJoueurMain) {
		this.panelJoueurMain = panelJoueurMain;
	}

	public JPanel getPanelTapis() {
		return panelTapis;
	}

	public void setPanelTapis(JPanel panelTapis) {
		this.panelTapis = panelTapis;
	}

	public JPanel getPanelInformations() {
		return panelInformations;
	}

	public void setPanelInformations(JPanel panelInformations) {
		this.panelInformations = panelInformations;
	}

	public JLabel getJoueur() {
		return joueur;
	}

	public void setJoueur(JLabel joueur) {
		this.joueur = joueur;
	}

	public JLabel getTemps() {
		return temps;
	}

	public void setTemps(JLabel temps) {
		this.temps = temps;
	}

	public JLabel getJoueurRestant() {
		return joueurRestant;
	}

	public void setJoueurRestant(JLabel joueurRestant) {
		this.joueurRestant = joueurRestant;
	}

	public JScrollPane getPanelScrollTapis() {
		return panelScrollTapis;
	}

	public void setPanelScrollTapis(JScrollPane panelScrollTapis) {
		this.panelScrollTapis = panelScrollTapis;
	}

	public JScrollPane getPanelScrollJoueurMain() {
		return panelScrollJoueurMain;
	}

	public void setPanelScrollJoueurMain(JScrollPane panelScrollJoueurMain) {
		this.panelScrollJoueurMain = panelScrollJoueurMain;
	}
}