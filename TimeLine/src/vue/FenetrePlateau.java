package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

public class FenetrePlateau extends JFrame {

	private static final long serialVersionUID = 6858191762218718778L;
	private PanelJeu panelJeu;
	private MenuEnregistrer menuEnregistrer;

	public FenetrePlateau() {
		this.setPreferredSize(new Dimension(1200, 850));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.panelJeu = new PanelJeu();
		this.menuEnregistrer = new MenuEnregistrer();
		this.setJMenuBar(this.menuEnregistrer);
		this.add(panelJeu, BorderLayout.CENTER);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public PanelJeu getPanelJeu() {
		return panelJeu;
	}

	public void setPanelJeu(PanelJeu panelJeu) {
		this.panelJeu = panelJeu;
	}

	public MenuEnregistrer getMenuEnregistrer() {
		return menuEnregistrer;
	}

	public void setMenuEnregistrer(MenuEnregistrer menuEnregistrer) {
		this.menuEnregistrer = menuEnregistrer;
	}
}