package vue;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import modele.enumJeu.Jeu;

public class FenetreAjoutCarte extends JFrame {

	private static final long serialVersionUID = 333676741126370969L;
	private JPanel panelJeu;
	private JPanel panelTimeLine;
	private JPanel panelBouton;
	private JLabel jeu;
	private JLabel titre;
	private JComboBox<Jeu> choixJeu;
	private JLabel invention;
	private JTextField textInvention;
	private JLabel dateInvention;
	private JTextField textDateInvention;
	private JButton imageCache;
	private JButton imageVisible;
	private JLabel nomImage;
	private JTextField textNomImage;
	private JLabel pays;
	private JTextField textPays;
	private JLabel PIB;
	private JTextField textPIB;
	private JButton ajouter;
	private JLabel superficie;
	private JTextField textSuperficie;
	private JLabel pollution;
	private JTextField textPollution;
	private JLabel population;
	private JTextField textPopulation;

	public FenetreAjoutCarte() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle("Jeux");
		this.getContentPane().setLayout(new GridLayout(4, 1));
		this.titre = new JLabel("Ajout de cartes");
		this.getContentPane().add(this.titre);
		this.choixJeu = new JComboBox<Jeu>();
		this.choixJeu.addItem(Jeu.timeline);
		this.choixJeu.addItem(Jeu.cardline);
		this.jeu = new JLabel("Choix du jeu");
		this.panelJeu = new JPanel();
		this.panelJeu.add(jeu);
		this.panelJeu.add(choixJeu);
		this.getContentPane().add(panelJeu);
		this.invention = new JLabel("Invention");
		this.textInvention = new JTextField();
		this.dateInvention = new JLabel("Date d'invention");
		this.textDateInvention = new JTextField();
		this.nomImage = new JLabel("Nom de l'image");
		this.textNomImage = new JTextField();
		this.imageCache = new JButton("Image cachée...");
		this.imageVisible = new JButton("Image visible...");
		this.pays = new JLabel("Pays");
		this.textPays = new JTextField();
		this.PIB = new JLabel("PIB");
		this.textPIB = new JTextField();
		this.pollution = new JLabel("Pollution");
		this.textPollution = new JTextField();
		this.superficie = new JLabel("Superficie");
		this.textSuperficie = new JTextField();
		this.population = new JLabel("Population");
		this.textPopulation = new JTextField();
		panelJeu();
		this.panelBouton = new JPanel();
		this.ajouter = new JButton("Ajouter");
		this.panelBouton.add(ajouter);
		this.add(panelBouton);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void panelJeu() {
		this.panelTimeLine = new JPanel();
		this.panelTimeLine.setLayout(new GridLayout(5, 2));
		this.panelTimeLine.add(invention);
		this.panelTimeLine.add(textInvention);
		this.panelTimeLine.add(dateInvention);
		this.panelTimeLine.add(textDateInvention);
		this.panelTimeLine.add(nomImage);
		this.panelTimeLine.add(textNomImage);
		this.panelTimeLine.add(imageCache);
		this.panelTimeLine.add(imageVisible);
		this.panelTimeLine.add(pays);
		this.pays.setVisible(false);
		this.panelTimeLine.add(textPays);
		this.textPays.setVisible(false);
		this.panelTimeLine.add(PIB);
		this.PIB.setVisible(false);
		this.panelTimeLine.add(textPIB);
		this.textPIB.setVisible(false);
		this.panelTimeLine.add(pollution);
		this.pollution.setVisible(false);
		this.panelTimeLine.add(textPollution);
		this.textPollution.setVisible(false);
		this.panelTimeLine.add(superficie);
		this.superficie.setVisible(false);
		this.panelTimeLine.add(textSuperficie);
		this.textSuperficie.setVisible(false);
		this.panelTimeLine.add(population);
		this.population.setVisible(false);
		this.panelTimeLine.add(textPopulation);
		this.textPopulation.setVisible(false);
		this.add(panelTimeLine);
	}

	public JPanel getPanelJeu() {
		return panelJeu;
	}

	public void setPanelJeu(JPanel panelJeu) {
		this.panelJeu = panelJeu;
	}

	public JPanel getPanelJoueur() {
		return panelTimeLine;
	}

	public void setPanelJoueur(JPanel panelTimeLine) {
		this.panelTimeLine = panelTimeLine;
	}

	public JPanel getPanelBouton() {
		return panelBouton;
	}

	public void setPanelBouton(JPanel panelBouton) {
		this.panelBouton = panelBouton;
	}

	public JLabel getJeu() {
		return jeu;
	}

	public void setJeu(JLabel jeu) {
		this.jeu = jeu;
	}

	public JLabel getTitre() {
		return titre;
	}

	public void setTitre(JLabel titre) {
		this.titre = titre;
	}

	public JComboBox<Jeu> getChoixJeu() {
		return choixJeu;
	}

	public void setChoixJeu(JComboBox<Jeu> choixJeu) {
		this.choixJeu = choixJeu;
	}

	public JLabel getInvention() {
		return invention;
	}

	public void setInvention(JLabel invention) {
		this.invention = invention;
	}

	public JTextField getTextInvention() {
		return textInvention;
	}

	public void setTextInvention(JTextField textInvention) {
		this.textInvention = textInvention;
	}

	public JLabel getDateInvention() {
		return dateInvention;
	}

	public void setDateInvention(JLabel dateInvention) {
		this.dateInvention = dateInvention;
	}

	public JTextField getTextDateInvention() {
		return textDateInvention;
	}

	public void setTextDateInvention(JTextField textDateInvention) {
		this.textDateInvention = textDateInvention;
	}

	public JButton getImageCache() {
		return imageCache;
	}

	public void setImageCache(JButton imageCache) {
		this.imageCache = imageCache;
	}

	public JButton getImageVisible() {
		return imageVisible;
	}

	public void setImageVisible(JButton imageVisible) {
		this.imageVisible = imageVisible;
	}

	public JLabel getNomImage() {
		return nomImage;
	}

	public void setNomImage(JLabel nomImage) {
		this.nomImage = nomImage;
	}

	public JTextField getTextNomImage() {
		return textNomImage;
	}

	public void setTextNomImage(JTextField textNomImage) {
		this.textNomImage = textNomImage;
	}

	public JButton getAjouter() {
		return ajouter;
	}

	public void setAjouter(JButton ajouter) {
		this.ajouter = ajouter;
	}

	public JPanel getPanelTimeLine() {
		return panelTimeLine;
	}

	public void setPanelTimeLine(JPanel panelTimeLine) {
		this.panelTimeLine = panelTimeLine;
	}

	public JLabel getPays() {
		return pays;
	}

	public void setPays(JLabel pays) {
		this.pays = pays;
	}

	public JTextField getTextPays() {
		return textPays;
	}

	public void setTextPays(JTextField textPays) {
		this.textPays = textPays;
	}

	public JLabel getPIB() {
		return PIB;
	}

	public void setPIB(JLabel pIB) {
		PIB = pIB;
	}

	public JTextField getTextPIB() {
		return textPIB;
	}

	public void setTextPIB(JTextField textPIB) {
		this.textPIB = textPIB;
	}

	public JLabel getSuperficie() {
		return superficie;
	}

	public void setSuperficie(JLabel superficie) {
		this.superficie = superficie;
	}

	public JTextField getTextSuperficie() {
		return textSuperficie;
	}

	public void setTextSuperficie(JTextField textSuperficie) {
		this.textSuperficie = textSuperficie;
	}

	public JLabel getPollution() {
		return pollution;
	}

	public void setPollution(JLabel pollution) {
		this.pollution = pollution;
	}

	public JTextField getTextPollution() {
		return textPollution;
	}

	public void setTextPollution(JTextField textPollution) {
		this.textPollution = textPollution;
	}

	public JLabel getPopulation() {
		return population;
	}

	public void setPopulation(JLabel population) {
		this.population = population;
	}

	public JTextField getTextPopulation() {
		return textPopulation;
	}

	public void setTextPopulation(JTextField textPopulation) {
		this.textPopulation = textPopulation;
	}
}