package vue;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;

import modele.ModelJTableVoirScore;
import modele.Plateau;

public class FenetreVoirScore extends JFrame {

	private static final long serialVersionUID = 7792558035773949753L;
	private JTable tableau;

	public FenetreVoirScore(Plateau plateau) {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle("Scores");
		this.setResizable(false);
		this.tableau = new JTable(new ModelJTableVoirScore(plateau));
		this.tableau.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		this.tableau.setGridColor(Color.BLACK);
		this.tableau.setAutoCreateRowSorter(true);
		this.getContentPane().add(new JScrollPane(tableau));
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}