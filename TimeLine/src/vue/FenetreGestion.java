package vue;

import java.awt.GridLayout;
import java.util.Date;
import java.util.Properties;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import modele.DateLabelFormatter;
import modele.Joueur;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

public class FenetreGestion extends JFrame {

	private static final long serialVersionUID = 2125893644370096131L;
	private JLabel titre;
	private JPanel panelJeuTitre;
	private JPanel panelJeu;
	private JPanel panelJoueurTitre;
	private JPanel panelJoueur;
	private JPanel panelJoueurList;
	private JPanel panelJoueurBouton;
	private JPanel panelJeuBouton;
	private JScrollPane scrollJoueurs;
	private JLabel jeu;
	private JComboBox<Jeu> choixJeu;
	private JLabel theme;
	private JComboBox<ThemeCarte> choixTheme;
	private JLabel joueur;
	private JLabel pseudo;
	private JTextField textPseudo;
	private JLabel dateNaissance;
	private JDatePickerImpl textDateNaissance;
	private JList<Joueur> ensembleJoueur;
	private DefaultListModel<Joueur> modelListJoueur;
	private JButton ajouter;
	private JButton supprimer;
	private JButton modifier;
	private JButton annuler;
	private JButton lancerPartie;
	private MenuOuvrir menuOuvrir;

	public FenetreGestion() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Jeux");
		this.menuOuvrir = new MenuOuvrir();
		this.setJMenuBar(this.menuOuvrir);
		initComboJeu();
		initComboTheme();
		this.setLayout(new GridLayout(7, 1));
		initPanelJeuTitre();
		initPanelJeu();
		initPanelJoueurTitre();
		initPanelJoueur();
		initPanelJoueurList();
		initPanelJoueurBouton();
		initPanelJeuBouton();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initComboJeu() {
		this.choixJeu = new JComboBox<Jeu>();
		this.choixJeu.addItem(Jeu.timeline);
		this.choixJeu.addItem(Jeu.cardline);
	}

	private void initComboTheme() {
		this.choixTheme = new JComboBox<ThemeCarte>();
		this.choixTheme.addItem(ThemeCarte.pib);
		this.choixTheme.addItem(ThemeCarte.pollution);
		this.choixTheme.addItem(ThemeCarte.population);
		this.choixTheme.addItem(ThemeCarte.superficie);
		this.choixTheme.setEnabled(false);
	}

	private void initPanelJeu() {
		initComboJeu();
		initComboTheme();
		this.theme = new JLabel("Choix du thème");
		this.jeu = new JLabel("Choix du jeu");
		this.panelJeu = new JPanel();
		this.panelJeu.setLayout(new GridLayout(2, 2));
		this.panelJeu.add(jeu);
		this.panelJeu.add(choixJeu);
		this.panelJeu.add(theme);
		this.panelJeu.add(choixTheme);
		this.add(panelJeu);
	}

	private void initPanelJeuTitre() {
		this.titre = new JLabel("Gestionnaire de jeu");
		this.panelJeuTitre = new JPanel();
		this.panelJeuTitre.add(this.titre, SwingConstants.CENTER);
		this.add(panelJeuTitre);
	}

	private void initPanelJoueur() {
		this.pseudo = new JLabel("Pseudo");
		this.textPseudo = new JTextField();
		this.dateNaissance = new JLabel("Date de naissance");
		this.textDateNaissance = new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), new Properties()),
				new DateLabelFormatter());
		this.panelJoueur = new JPanel();
		this.panelJoueur.setLayout(new GridLayout(2, 2));
		this.panelJoueur.add(pseudo);
		this.panelJoueur.add(textPseudo);
		this.panelJoueur.add(dateNaissance);
		this.panelJoueur.add(textDateNaissance);
		this.add(panelJoueur);
	}

	private void initPanelJoueurTitre() {
		this.joueur = new JLabel("Information joueur");
		this.panelJoueurTitre = new JPanel();
		this.panelJoueurTitre.add(joueur, SwingConstants.CENTER);
		this.add(panelJoueurTitre);
	}

	private void initPanelJoueurList() {
		this.panelJoueurList = new JPanel();
		this.ensembleJoueur = new JList<Joueur>();
		this.ensembleJoueur.setPrototypeCellValue(new Joueur("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX", new Date()));
		this.ensembleJoueur.setVisibleRowCount(3);
		this.scrollJoueurs = new JScrollPane();
		this.modelListJoueur = new DefaultListModel<Joueur>();
		this.scrollJoueurs.setViewportView(ensembleJoueur);
		this.ensembleJoueur.setModel(modelListJoueur);
		this.panelJoueurList.add(this.scrollJoueurs);
		this.add(panelJoueurList);
	}

	private void initPanelJoueurBouton() {
		this.panelJoueurBouton = new JPanel();
		this.ajouter = new JButton("Ajouter");
		this.supprimer = new JButton("Supprimer");
		this.modifier = new JButton("Modifier");
		this.annuler = new JButton("Annuler");
		this.annuler.setVisible(false);
		this.modifier.setVisible(false);
		this.supprimer.setVisible(false);
		this.panelJoueurBouton.add(ajouter);
		this.panelJoueurBouton.add(annuler);
		this.panelJoueurBouton.add(modifier);
		this.panelJoueurBouton.add(supprimer);
		this.add(panelJoueurBouton);
	}

	public JButton getModifier() {
		return modifier;
	}

	public void setModifier(JButton modifier) {
		this.modifier = modifier;
	}

	public JButton getAnnuler() {
		return annuler;
	}

	public void setAnnuler(JButton annuler) {
		this.annuler = annuler;
	}

	public void setAjouter(JButton ajouter) {
		this.ajouter = ajouter;
	}

	public void setSupprimer(JButton supprimer) {
		this.supprimer = supprimer;
	}

	private void initPanelJeuBouton() {
		this.panelJeuBouton = new JPanel();
		this.lancerPartie = new JButton("Lancer partie");
		this.panelJeuBouton.add(lancerPartie, SwingConstants.CENTER);
		this.add(panelJeuBouton);
	}

	public JLabel getTitre() {
		return titre;
	}

	public void setTitre(JLabel titre) {
		this.titre = titre;
	}

	public JPanel getPanelJeuTitre() {
		return panelJeuTitre;
	}

	public void setPanelJeuTitre(JPanel panelJeuTitre) {
		this.panelJeuTitre = panelJeuTitre;
	}

	public JPanel getPanelJeu() {
		return panelJeu;
	}

	public void setPanelJeu(JPanel panelJeu) {
		this.panelJeu = panelJeu;
	}

	public JPanel getPanelJoueurTitre() {
		return panelJoueurTitre;
	}

	public void setPanelJoueurTitre(JPanel panelJoueurTitre) {
		this.panelJoueurTitre = panelJoueurTitre;
	}

	public JPanel getPanelJoueur() {
		return panelJoueur;
	}

	public void setPanelJoueur(JPanel panelJoueur) {
		this.panelJoueur = panelJoueur;
	}

	public JPanel getPanelJoueurList() {
		return panelJoueurList;
	}

	public void setPanelJoueurList(JPanel panelJoueurList) {
		this.panelJoueurList = panelJoueurList;
	}

	public JPanel getPanelJoueurBouton() {
		return panelJoueurBouton;
	}

	public void setPanelJoueurBouton(JPanel panelJoueurBouton) {
		this.panelJoueurBouton = panelJoueurBouton;
	}

	public JPanel getPanelJeuBouton() {
		return panelJeuBouton;
	}

	public void setPanelJeuBouton(JPanel panelJeuBouton) {
		this.panelJeuBouton = panelJeuBouton;
	}

	public JScrollPane getScrollJoueurs() {
		return scrollJoueurs;
	}

	public void setScrollJoueurs(JScrollPane scrollJoueurs) {
		this.scrollJoueurs = scrollJoueurs;
	}

	public JLabel getJeu() {
		return jeu;
	}

	public void setJeu(JLabel jeu) {
		this.jeu = jeu;
	}

	public JComboBox<Jeu> getChoixJeu() {
		return choixJeu;
	}

	public void setChoixJeu(JComboBox<Jeu> choixJeu) {
		this.choixJeu = choixJeu;
	}

	public JLabel getTheme() {
		return theme;
	}

	public void setTheme(JLabel theme) {
		this.theme = theme;
	}

	public JComboBox<ThemeCarte> getChoixTheme() {
		return choixTheme;
	}

	public void setChoixTheme(JComboBox<ThemeCarte> choixTheme) {
		this.choixTheme = choixTheme;
	}

	public JLabel getJoueur() {
		return joueur;
	}

	public void setJoueur(JLabel joueur) {
		this.joueur = joueur;
	}

	public JLabel getPseudo() {
		return pseudo;
	}

	public void setPseudo(JLabel pseudo) {
		this.pseudo = pseudo;
	}

	public JTextField getTextPseudo() {
		return textPseudo;
	}

	public void setTextPseudo(JTextField textPseudo) {
		this.textPseudo = textPseudo;
	}

	public JLabel getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(JLabel dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public JDatePickerImpl getTextDateNaissance() {
		return textDateNaissance;
	}

	public void setTextDateNaissance(JDatePickerImpl textDateNaissance) {
		this.textDateNaissance = textDateNaissance;
	}

	public JList<Joueur> getEnsembleJoueur() {
		return ensembleJoueur;
	}

	public void setEnsembleJoueur(JList<Joueur> ensembleJoueur) {
		this.ensembleJoueur = ensembleJoueur;
	}

	public DefaultListModel<Joueur> getModelListJoueur() {
		return modelListJoueur;
	}

	public void setModelListJoueur(DefaultListModel<Joueur> modelListJoueur) {
		this.modelListJoueur = modelListJoueur;
	}

	public JButton getLancerPartie() {
		return lancerPartie;
	}

	public void setLancerPartie(JButton lancerPartie) {
		this.lancerPartie = lancerPartie;
	}

	public JButton getAjouter() {
		return ajouter;
	}

	public JButton getSupprimer() {
		return supprimer;
	}

	public MenuOuvrir getMenuOuvrir() {
		return menuOuvrir;
	}

	public void setMenuOuvrir(MenuOuvrir menuOuvrir) {
		this.menuOuvrir = menuOuvrir;
	}
}