package vue;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import modele.Carte;

public class FenetreCarteVisible extends JFrame {

	private static final long serialVersionUID = 1837264978225606943L;

	public FenetreCarteVisible(Carte carte) {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle(carte.getLibelle());
		this.setResizable(false);
		JButton bouton = new JButton(new ImageIcon(carte.getIllustrationVisible()));
		bouton.setFocusable(false);
		bouton.setSelected(false);
		this.add(bouton);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}