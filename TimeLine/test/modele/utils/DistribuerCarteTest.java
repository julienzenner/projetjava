package modele.utils;

import java.io.IOException;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import modele.Joueur;
import modele.Plateau;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

class DistribuerCarteTest {

	@Before
	public Plateau initialisation(int nbJoueurs, Jeu jeu) throws IOException {
		Plateau plateau = new Plateau();
		for (int i = 0; i < nbJoueurs; i++) {
			plateau.addJoueurs(new Joueur("test" + i, new Date()));
		}
		plateau.setTypeJeu(jeu);
		plateau.chargerCarte(ThemeCarte.pib);
		plateau.distribuerCarte();
		return plateau;
	}

	@Test
	void testAvec2Joueurs() throws IOException {
		Plateau plateauCardLine = initialisation(2, Jeu.cardline);
		Assertions.assertThat(plateauCardLine.getJoueurs(0).getMain().size()).isEqualTo(6);
		Assertions.assertThat(plateauCardLine.getPaquet().size()).isEqualTo(48);
	}

	@Test
	void testAvec3Joueurs() throws IOException {
		Plateau plateauTimeLine = initialisation(3, Jeu.timeline);
		Assertions.assertThat(plateauTimeLine.getJoueurs(0).getMain().size()).isEqualTo(6);
		Assertions.assertThat(plateauTimeLine.getPaquet().size()).isEqualTo(42);
	}

	@Test
	void testAvec4Joueurs() throws IOException {
		Plateau plateauCardLine = initialisation(4, Jeu.cardline);
		Assertions.assertThat(plateauCardLine.getJoueurs(0).getMain().size()).isEqualTo(5);
		Assertions.assertThat(plateauCardLine.getPaquet().size()).isEqualTo(40);
	}

	@Test
	void testAvec5Joueurs() throws IOException {
		Plateau plateauTimeLine = initialisation(5, Jeu.timeline);
		Assertions.assertThat(plateauTimeLine.getJoueurs(0).getMain().size()).isEqualTo(5);
		Assertions.assertThat(plateauTimeLine.getPaquet().size()).isEqualTo(35);
	}

	@Test
	void testAvec6Joueurs() throws IOException {
		Plateau plateauCardLine = initialisation(6, Jeu.cardline);
		Assertions.assertThat(plateauCardLine.getJoueurs(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(plateauCardLine.getPaquet().size()).isEqualTo(36);
	}

	@Test
	void testAvec7Joueurs() throws IOException {
		Plateau plateauTimeLine = initialisation(7, Jeu.timeline);
		Assertions.assertThat(plateauTimeLine.getJoueurs(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(plateauTimeLine.getPaquet().size()).isEqualTo(32);
	}

	@Test
	void testAvec8Joueurs() throws IOException {
		Plateau plateauCardLine = initialisation(8, Jeu.cardline);
		Assertions.assertThat(plateauCardLine.getJoueurs(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(plateauCardLine.getPaquet().size()).isEqualTo(28);
	}
}