package modele.utils;

import java.io.IOException;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import modele.Joueur;
import modele.Plateau;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

class DebutPartieTest {

	@Before
	public Plateau initialisation() throws IOException {
		Plateau plateau = new Plateau();
		for (int i = 0; i < 2; i++) {
			plateau.addJoueurs(new Joueur("test" + i, new Date()));
		}
		plateau.setTypeJeu(Jeu.cardline);
		plateau.chargerCarte(ThemeCarte.pib);
		plateau.distribuerCarte();
		return plateau;
	}

	@Test
	void testDebutPartie() throws IOException {
		Plateau plateauCardLine = initialisation();
		plateauCardLine.debutPartie();
		Assertions.assertThat(plateauCardLine.getFriseChronologique(0)).isNotIn(plateauCardLine.getPaquet());
		Assertions.assertThat(plateauCardLine.getFriseChronologique().size()).isEqualTo(1);
	}
}