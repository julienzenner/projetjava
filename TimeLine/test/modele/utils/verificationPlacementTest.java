package modele.utils;

import java.io.IOException;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import modele.CarteTimeline;
import modele.Joueur;
import modele.Plateau;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

class verificationPlacementTest {

	@Before
	public Plateau initialisation(Jeu jeu, ThemeCarte theme) throws IOException {
		Plateau plateau = new Plateau();
		for (int i = 0; i < 2; i++) {
			plateau.addJoueurs(new Joueur("test" + i, new Date()));
		}
		plateau.setTypeJeu(jeu);
		plateau.chargerCarte(theme);
		plateau.distribuerCarte();
		return plateau;
	}

	@Test
	void testVerificationPlacementTimeLineSimple() throws IOException {
		Plateau plateau = initialisation(Jeu.timeline, null);
		plateau.addFriseChronologique(new CarteTimeline("", 0, "", "", 0.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", -9999, "", "", -9999.0));
		plateau.addFriseChronologique(0, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1))).isTrue();
	}

	@Test
	void testVerificationPlacementTimeLine1() throws IOException {
		Plateau plateau = initialisation(Jeu.timeline, null);
		plateau.addFriseChronologique(new CarteTimeline("", 0, "", "", 0.0));
		plateau.addFriseChronologique(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", -9999, "", "", -9999.0));
		plateau.addFriseChronologique(0, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1))).isTrue();
	}

	@Test
	void testVerificationPlacementTimeLine2() throws IOException {
		Plateau plateau = initialisation(Jeu.timeline, null);
		plateau.addFriseChronologique(new CarteTimeline("", 0, "", "", 0.0));
		plateau.addFriseChronologique(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", 100, "", "", 100.0));
		plateau.addFriseChronologique(1, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1))).isTrue();
	}

	@Test
	void testVerificationPlacementTimeLine3() throws IOException {
		Plateau plateau = initialisation(Jeu.timeline, null);
		plateau.addFriseChronologique(new CarteTimeline("", 0, "", "", 0.0));
		plateau.addFriseChronologique(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", 1000, "", "", 1000.0));
		plateau.addFriseChronologique(1, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 2));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 2))).isTrue();
		plateau.addFriseChronologique(3, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1))).isTrue();
	}

	@Test
	void testVerificationPlacementTimeLine4() throws IOException {
		Plateau plateau = initialisation(Jeu.timeline, null);
		plateau.addFriseChronologique(new CarteTimeline("", 0, "", "", 0.0));
		plateau.addFriseChronologique(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", 100, "", "", 100.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", 1000, "", "", 1000.0));
		plateau.getJoueurs(0).addMain(new CarteTimeline("", -1000, "", "", -1000.0));
		plateau.addFriseChronologique(1, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 3));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 3))).isTrue();
		plateau.addFriseChronologique(3, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 2));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 2))).isTrue();
		plateau.addFriseChronologique(0, plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1));
		Assertions.assertThat(plateau.verificationPlacement(plateau.getJoueurs(0),
				plateau.getJoueurs(0).getMain(plateau.getJoueurs(0).getMain().size() - 1))).isTrue();
		Assertions.assertThat(plateau.getFriseChronologique().size()).isEqualTo(5);
	}
}