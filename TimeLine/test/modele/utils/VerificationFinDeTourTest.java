package modele.utils;

import java.io.IOException;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import modele.Joueur;
import modele.Plateau;
import modele.enumJeu.Jeu;
import modele.enumJeu.ThemeCarte;

class VerificationFinDeTourTest {

	@Before
	public Plateau initialisation() throws IOException {
		Plateau plateau = new Plateau();
		for (int i = 0; i < 8; i++) {
			plateau.addJoueurs(new Joueur("test" + i, new Date()));
		}
		plateau.setTypeJeu(Jeu.cardline);
		plateau.chargerCarte(ThemeCarte.pib);
		return plateau;
	}

	@Test
	void verificationFinDeTourAvecTous0Carte() throws IOException {
		Plateau plateau = initialisation();
		plateau.verificationFinDeTour();
		Assertions.assertThat(plateau.getJoueurs().size()).isEqualTo(8);
		Assertions.assertThat(plateau.getJoueurs(0).getMain().size()).isEqualTo(1);
		Assertions.assertThat(plateau.getJoueursPerdant().size()).isEqualTo(0);
	}

	@Test
	void verificationFinDeTourAvecCertainsAvec0Carte() throws IOException {
		Plateau plateau = initialisation();
		plateau.distribuerCarte();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < plateau.getJoueurs(3).getMain().size(); j++) {
				plateau.getJoueurs(i).getMain().remove(0);
			}
		}
		plateau.verificationFinDeTour();
		Assertions.assertThat(plateau.getJoueurs().size()).isEqualTo(2);
		Assertions.assertThat(plateau.getJoueursPerdant().size()).isEqualTo(6);
		Assertions.assertThat(plateau.getDefausse().size()).isEqualTo(24);
	}

	@Test
	void verificationFinDeTourAvecCartes() throws IOException {
		Plateau plateau = initialisation();
		plateau.distribuerCarte();
		plateau.verificationFinDeTour();
		Assertions.assertThat(plateau.getJoueurs().size()).isEqualTo(8);
		Assertions.assertThat(plateau.getJoueursPerdant().size()).isEqualTo(0);
		Assertions.assertThat(plateau.getDefausse().size()).isEqualTo(0);
	}

	@Test
	void verificationFinDeTourAvec1Gagnant() throws IOException {
		Plateau plateau = initialisation();
		plateau.distribuerCarte();
		for (int j = 0; j < plateau.getJoueurs(3).getMain().size(); j++) {
			plateau.getJoueurs(0).getMain().remove(0);
		}
		plateau.verificationFinDeTour();
		Assertions.assertThat(plateau.getJoueurs().size()).isEqualTo(1);
		Assertions.assertThat(plateau.getJoueursPerdant().size()).isEqualTo(7);
		Assertions.assertThat(plateau.getDefausse().size()).isEqualTo(28);
	}
}